Les coquilles trouvées sur la page bibliographie chronologique du musée virtuel des dictionnaires

1992
DUGAS André, MOLINIER Christian, (Dir.), La **productivitié** lexicale, Langue française n° 96.

1994
LECOINTRE Simone, LEEMAN Danielle, (Dir.), Le lexique : construire l’interprétation, **Langage** n ° 103, Larousse. La collection se nomme Langue française et non Langage

1998
QUEMADA Bernard, PRUVOST Jean, (Dir.), Les Dictionnaires de l’Académie française et la lexicographie **intstitutionnelle** européenne, Actes du Colloque international, Collection Lexica n° 2, 566 p.



