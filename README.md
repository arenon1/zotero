**Objectif du travail** 
Réaliser un inventaire des ressources lexicographiques publié sur le site du musée virtuel des dictionnaires sur le logiciel Zotero (tâche inscrite dans le cadre du partenariat avec le réseau METALEX)

La bibliothèque Zotero contient les **194 références** de la bibliographie chronologique "sommaire et indicative (1950-2000) des ouvrages utilisés relevant des domaines de la lexicologie, lexicographie, dictionnairique et métalexicographie par Jean PRUVOST (Université de Cergy-Pontoise)" consultable sur le lien suivant : https://dictionnaires.u-cergy.fr/biblio/mvd._bibliographie.html. 

Utilisation de l'extension Zotero Connector 